//
//  Created by Pablo Balduz on 14/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public struct LoginResponse: Decodable {
    public let accessToken: String
    public let refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token", refreshToken = "refresh_token"
    }
}
