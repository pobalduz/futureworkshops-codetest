//
//  Created by Pablo Balduz on 14/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public typealias ID = Int

public struct ArticlePreview: Decodable, DateDecodable {
    public let id: ID
    public let date: Date
    public let summary: String
    public let thumbnailTemplateURL: URL
    public let thumbnailURL: URL
    public let title: String
    
    static var dateFormatter: DateFormatter { appAPIDateFormatter }
    
    enum CodingKeys: String, CodingKey {
        case id, date, summary, thumbnailTemplateURL = "thumbnail_template_url", thumbnailURL = "thumbnail_url", title
    }
}

// Testing purposes
extension ArticlePreview {
    public init(id: ID, date: Date, summary: String, title: String, thumbnailTemplateURL: URL, thumbnailURL: URL) {
        self.id = id
        self.date = date
        self.summary = summary
        self.title = title
        self.thumbnailTemplateURL = thumbnailTemplateURL
        self.thumbnailURL = thumbnailURL
    }
}

public struct ArticleDetails: Decodable, DateDecodable {
    public let id: ID
    public let title: String
    public let summary: String
    public let content: String
    public let date: Date
    public let thumbnailURL: URL
    public let thumbnailTemplateURL: URL
    public let imageURL: URL
    public let sourceURL: URL
    
    static var dateFormatter: DateFormatter { appAPIDateFormatter }
    
    enum CodingKeys: String, CodingKey {
        case id, title, summary, content, date, thumbnailURL = "thumbnail_url", thumbnailTemplateURL = "thumbnail_template_url", imageURL = "image_url", sourceURL = "source_url"
    }
}

// Testing purposes
extension ArticleDetails {
    public init(id: ID, date: Date, summary: String, title: String, thumbnailTemplateURL: URL, thumbnailURL: URL, content: String, imageURL: URL, sourceURL: URL) {
        self.id = id
        self.date = date
        self.summary = summary
        self.title = title
        self.thumbnailTemplateURL = thumbnailTemplateURL
        self.thumbnailURL = thumbnailURL
        self.content = content
        self.imageURL = imageURL
        self.sourceURL = sourceURL
    }
}

var appAPIDateFormatter: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter
}
