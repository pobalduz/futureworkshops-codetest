//
//  Created by Pablo Balduz on 15/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public class Keychain {
    
    public static let `default` = Keychain()
    
    public func authToken() -> String? {
        self[.authToken]
    }
    
    public func setAuthToken(_ token: String?) {
        self[.authToken] = token
    }
    
    public func refreshToken() -> String? {
        self[.refreshToken]
    }
    
    public func setRefreshToken(_ token: String?) {
        self[.refreshToken] = token
    }
    
    public func clearKeychain() {
        self[.authToken] = nil
        self[.refreshToken] = nil
    }
}

extension Keychain {
    
    enum Key: String {
        case authToken
        case refreshToken
    }
    
    subscript(key: Key) -> String? {
        get {
            guard let data = try? KeychainWrapper.get(account: key.rawValue) else {
                return nil
            }
            return String(data: data, encoding: .utf8)
        }

        set {
            if let value = newValue,
                let data = value.data(using: .utf8) {
                try? KeychainWrapper.set(value: data, account: key.rawValue)
            } else {
                try? KeychainWrapper.delete(account: key.rawValue)
            }
        }
    }
}
