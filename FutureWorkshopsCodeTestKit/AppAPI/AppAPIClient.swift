//
//  Created by Pablo Balduz on 14/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public class AppAPIClient: APIClient {
    
    public func login(userName: String, password: String, completionHandler: @escaping (Result<LoginResponse, Swift.Error>) -> ()) {
        let request = Request<LoginResponse>(endpoint: API.AppEndpoint.login(userName, password))
        execute(request: request) { result in
            let mappedResult = result.mapError { error -> Swift.Error in
                if case let Error.statusCodeFailure(statusCode, _) = error, statusCode == 403 {
                    return AuthError.invalidCredentials
                }
                return error
            }
            completionHandler(mappedResult)
        }
    }
    
    public func fetchArticles(completionHandler: @escaping (Result<[ArticlePreview], Swift.Error>) -> ()) {
        let request = Request<[ArticlePreview]>(endpoint: API.AppEndpoint.articles)
        execute(request: request, handler: completionHandler)
    }
    
    public func fetchArticleDetails(id: ID, completionHandler: @escaping (Result<ArticleDetails, Swift.Error>) -> ()) {
        let request = Request<ArticleDetails>(endpoint: API.AppEndpoint.articleDetails(id))
        execute(request: request, handler: completionHandler)
    }
}

extension AppAPIClient {
    
    enum AuthError: LocalizedError {
        case invalidCredentials
        
        var errorDescription: String? {
            switch self {
            case .invalidCredentials:
                return "Invalid credentials"
            }
        }
    }
}


