//
//  Created by Pablo Balduz on 14/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public struct API {
    
    public enum AppEnvironment: Environment {
        case develop
        
        public var baseURL: URL {
            switch self {
            case .develop:
                return URL(string: "https://mobilecodetest.fws.dev/")!
            }
        }
    }
    
    enum AppEndpoint: Endpoint {
        case login(String, String)
        case articles
        case articleDetails(ID)
        
        var path: String {
            switch self {
            case .login:
                return "auth/token"
            case .articles:
                return "api/v1/articles"
            case .articleDetails(let id):
                return "api/v1/articles/\(id)"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .login:
                return .post
            case .articles, .articleDetails:
                return .get
            }
        }
        
        var parameters: [String : Any]? {
            switch self {
            case let .login(userName, password):
                return [
                    "username": userName,
                    "password": password,
                    "grant_type": "password"
                ]
            default:
                return nil
            }
        }
    }

}
