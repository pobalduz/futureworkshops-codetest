//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

class Request<Type> {
    let endpoint: Endpoint
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
}

public protocol Environment {
    var baseURL: URL { get }
}

protocol Endpoint {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any]? { get }
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}
