//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

public protocol APIClientDelegate: class {
    func didReceiveUnauthorizedErrorResponse()
}

public class APIClient {
    
    public weak var delegate: APIClientDelegate?
    public var authToken: String?
    
    private let session: SessionProtocol
    private let environment: Environment
    
    public init(environment: Environment, session: SessionProtocol? = nil) {
        self.environment = environment
        self.session = session ?? URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
    }
    
    func execute<T: Decodable>(request: Request<T>, handler: @escaping (Result<T, Swift.Error>) -> ()) {
        let executeTask: OperationTask<Endpoint, T> =
            self.request
            => sendRequest
            => validateResponse
            => parseData
        executeTask(request.endpoint, handler)
    }
        
    // MARK: - Private
    
    private func request(for endpoint: Endpoint, requestHandler: (Result<URLRequest, Swift.Error>) -> ()) {
        let url = environment.baseURL.appendingPathComponent(endpoint.path)
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue
        if let token = authToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        if let params = endpoint.parameters, !params.isEmpty {
            do {
                let data = try JSONSerialization.data(withJSONObject: params, options: [])
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = data
            } catch {
                requestHandler(.failure(Error.requestParametersEncodingFailed))
            }
        }
        requestHandler(.success(request))
    }
    
    private func sendRequest(_ request: URLRequest, responseHandler: @escaping (Result<(Data, HTTPURLResponse), Swift.Error>) -> ()) {
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                responseHandler(.failure(error!))
                return
            }
            guard let response = response as? HTTPURLResponse, let data = data else {
                responseHandler(.failure(Error.malformedResponse))
                return
            }
            responseHandler(.success((data, response)))
        }.resume()
    }
    
    private func validateResponse(_ response: (Data, HTTPURLResponse), dataHandler: @escaping (Result<Data, Swift.Error>) -> ()) {
        switch response.1.statusCode {
        case 200..<300:
            dataHandler(.success(response.0))
        case 401:
            delegate?.didReceiveUnauthorizedErrorResponse()
        default:
            dataHandler(.failure(Error.statusCodeFailure(response.1.statusCode, response.0)))
        }
    }
    
    private func parseData<T: Decodable>(_ data: Data, handler: (Result<T, Swift.Error>) -> ()) {
        let decoder = JSONDecoder()
        if let model = T.self as? DateDecodable.Type {
            decoder.dateDecodingStrategy = .formatted(model.dateFormatter)
        }
        do {
            let value = try decoder.decode(T.self, from: data)
            handler(.success(value))
        } catch {
            handler(.failure(error))
        }
    }
}

extension APIClient {
    public enum Error: LocalizedError {
        case requestParametersEncodingFailed
        case malformedResponse
        case statusCodeFailure(Int, Data)
        
        public var errorDescription: String? {
            switch self {
            case .requestParametersEncodingFailed:
                return "Parameters couldn't be encoded"
            case .malformedResponse:
                return "Response doesn't have correct format"
            case let .statusCodeFailure(statusCode, _):
                return "Server answered with \(statusCode) error"
            }
        }
    }
}

protocol DateDecodable {
    static var dateFormatter: DateFormatter { get }
}

extension Array: DateDecodable where Element: DateDecodable{
    static var dateFormatter: DateFormatter {
        Element.dateFormatter
    }
}
