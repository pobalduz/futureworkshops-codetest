//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

precedencegroup LeftPrecedence {
    associativity: left
}

infix operator => : LeftPrecedence

typealias OperationTask<T, U> = (T, @escaping (Result<U, Error>) -> ()) -> ()

func =><T, U, V> (lhs: @escaping OperationTask<T, U>, rhs: @escaping OperationTask<U, V>) -> OperationTask<T, V> {
    return { initialValue, handler in
        lhs(initialValue) { result in
            switch result {
            case .failure(let error):
                handler(.failure(error))
            case .success(let value):
                rhs(value) { result2 in
                    switch result2 {
                    case .failure(let error):
                        handler(.failure(error))
                    case .success(let value2):
                        handler(.success(value2))
                    }
                }
            }
        }
    }
}
