//
//  FutureWorkshopsCodeTestKit.h
//  FutureWorkshopsCodeTestKit
//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for FutureWorkshopsCodeTestKit.
FOUNDATION_EXPORT double FutureWorkshopsCodeTestKitVersionNumber;

//! Project version string for FutureWorkshopsCodeTestKit.
FOUNDATION_EXPORT const unsigned char FutureWorkshopsCodeTestKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FutureWorkshopsCodeTestKit/PublicHeader.h>


