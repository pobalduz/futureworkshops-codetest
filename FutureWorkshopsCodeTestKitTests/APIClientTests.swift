//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsCodeTestKit

class APIClientTests: XCTestCase {
    
    private let environment = TestEnvironment()
    private let session = TestSession()
    private var testAPIClient: APIClient!
    
    override func setUp() {
        super.setUp()
        testAPIClient = APIClient(environment: environment, session: session)
    }
    
    func testSuccessfulGetRequest() throws {
        let responseObject = Player(name: "Kobe Bryant", number: 24)
        let responseData = try! JSONEncoder().encode(responseObject)
        let httpResponse = HTTPURLResponse(statusCode: 200)
        session.addResponse((responseData, httpResponse, nil))
        let request = Request<Player>(endpoint: TestEndpoint())
        testAPIClient.execute(request: request) { result in
            switch result {
            case .failure:
                XCTFail()
            case .success(let player):
                XCTAssertEqual(responseObject, player)
            }
        }
    }
    
    func testStatusCodeFailureRequest() throws {
        let responseData = "{ \"error\": \"400 error\" }".data(using: .utf8)
        let httpResponse = HTTPURLResponse(statusCode: 400)
        session.addResponse((responseData, httpResponse, nil))
        let request = Request<Player>(endpoint: TestEndpoint())
        testAPIClient.execute(request: request) { result in
            switch result {
            case .failure(let error):
                let apiError = error as? APIClient.Error
                XCTAssertNotNil(apiError)
                guard case let .statusCodeFailure(statusCode, data) = apiError else {
                    XCTFail()
                    return
                }
                XCTAssertEqual(statusCode, 400)
                XCTAssertEqual(responseData?.base64EncodedString(), data.base64EncodedString())
            case .success:
                XCTFail()
            }
        }
    }
}

extension HTTPURLResponse {
    convenience init(statusCode: Int) {
        self.init(url: URL(string: "http://www.apple.com")!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
    }
}

struct Player: Codable, Equatable {
    let name: String
    let number: Int
}
