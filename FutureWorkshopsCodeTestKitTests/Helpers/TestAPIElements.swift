//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

@testable import FutureWorkshopsCodeTestKit

struct TestEnvironment: Environment {
    var baseURL: URL {
        URL(string: "https://www.apple.com")!
    }
}

struct TestEndpoint: Endpoint {
    var path: String { "" }
    var method: HTTPMethod { .get }
    var parameters: [String : Any]? { nil }
}
