//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

@testable import FutureWorkshopsCodeTestKit

class TestSession: SessionProtocol {
    
    typealias URLSessionResponse = (Data?, URLResponse?, Error?)
    
    var responses = [URLSessionResponse]()
    
    func addResponse(_ response: URLSessionResponse) {
        responses.append(response)
    }
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return TestDataTask(response: responses.removeLast(), completionHandler: completionHandler)
    }
    
    private class TestDataTask: URLSessionDataTask {
        private let testResponse: URLSessionResponse
        private let completionHandler: (URLSessionResponse) -> ()
        
        init(response: URLSessionResponse, completionHandler: @escaping (URLSessionResponse) -> ()) {
            self.testResponse = response
            self.completionHandler = completionHandler
        }
        
        override func resume() {
            completionHandler((testResponse.0, testResponse.1, testResponse.2))
        }
    }
}
