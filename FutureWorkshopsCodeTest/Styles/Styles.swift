//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

// MARK: - UITextField

let baseTextFieldStyle: (String) -> (UITextField) -> Void = { placeholder in
    return { textField in
        textField.placeholder = placeholder
        textField.borderStyle = .roundedRect
    }
}

let userNameTextFieldStyle: (String) -> (UITextField) -> Void = { placeholder in
    baseTextFieldStyle(placeholder) <> { textField in
        textField.borderStyle = .roundedRect
        textField.textContentType = .username
        textField.autocapitalizationType = .none
    }
}

let passwordTextFieldStyle: (String) -> (UITextField) -> Void = { placeholder in
    baseTextFieldStyle(placeholder) <> { textField in
        textField.textContentType = .password
        textField.isSecureTextEntry = true
        textField.autocapitalizationType = .none
    }
}

// MARK: - UILabel

let multipleLinesLabelStyle: (UILabel) -> Void = { label in
    label.numberOfLines = 0
}

let dateLabelStyle: (UILabel) -> Void = { label in
    label.font = UIFont.preferredFont(forTextStyle: .footnote)
}

let titleLabelStyle: (UILabel) -> Void =
    multipleLinesLabelStyle <> { label in
        label.font = UIFont.preferredFont(forTextStyle: .body)
}

let boldTitleLabelStyle: (UILabel) -> Void =
    titleLabelStyle <> { label in
        let font = label.font.applyingWeight(.bold)
        label.font = font
}

let summaryPreviewLabelStyle: (UILabel) -> Void =
    multipleLinesLabelStyle <> { label in
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
}

let summaryDetailLabelStyle: (UILabel) -> Void =
    multipleLinesLabelStyle <> { label in
        label.font = UIFont.preferredFont(forTextStyle: .body)
}

let errorViewLabelStyle: (UILabel) -> Void =
    multipleLinesLabelStyle <> { label in
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textAlignment = .center
}

// MARK: - UIView

let roundedCornersStyle: (CGFloat) -> (UIView) -> Void = { radius in
    return { view in
        view.layer.cornerRadius = radius
        view.layer.masksToBounds = true
    }
}

// MARK: - UIImageView

let fillImageViewStyle: (UIImageView) -> Void = { imageView in
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
}

let previewImageViewStyle: (UIImageView) -> Void =
    roundedCornersStyle(8)
        <> fillImageViewStyle
