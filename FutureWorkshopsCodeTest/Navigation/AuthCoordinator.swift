//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

protocol AuthObserver: class {
    func didFinishAuthentication()
}

class AuthCoordinator: Coordinator {
    
    weak var observer: AuthObserver?
    
    private(set) var childCoordinators: [Coordinator] = []
    
    private let rootViewController: ContainerViewController
    
    init(rootViewController: ContainerViewController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        let viewController = LoginViewController()
        let viewModel = LoginViewModel()
        viewController.viewModel = viewModel
        viewModel.coordinator = self
        rootViewController.updateContentViewController(viewController)
    }
    
    func completeLogin() {
        observer?.didFinishAuthentication()
    }
}
