//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit
import FutureWorkshopsCodeTestKit

class AppCoordinator: Coordinator, AuthObserver, LogoutObserver, APIClientDelegate {
    private(set) var childCoordinators: [Coordinator] = []
    
    private let window: UIWindow
    private let rootViewController = ContainerViewController()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        window.rootViewController = rootViewController
        if Current.authProvider.hasActiveSession {
            showArticles()
        } else {
            showLogin()
        }
    }
    
    // MARK: - APIClientDelegate
    
    func didReceiveUnauthorizedErrorResponse() {
        rootViewController.showAlert(title: "Session expired", message: "Your session is no longer avaliable and you have to login again")
        Current.authProvider.logout()
        Current.articlesProvider.removeFavorites()
        childCoordinators.removeLast()
        showLogin()
    }
    
    // MARK: - AuthObserver
    
    func didFinishAuthentication() {
        childCoordinators.removeLast()
        showArticles()
    }
    
    // MARK: - LogoutObserver
    
    func didPerformLogout() {
        childCoordinators.removeLast()
        showLogin()
    }
    
    // MARK: - Private
    
    private func showLogin() {
        let authCoordinator = AuthCoordinator(rootViewController: rootViewController)
        authCoordinator.observer = self
        childCoordinators.append(authCoordinator)
        authCoordinator.start()
    }
    
    private func showArticles() {
        let articlesCoordinator = ArticlesCoordinator(rootViewController: rootViewController)
        articlesCoordinator.observer = self
        childCoordinators.append(articlesCoordinator)
        articlesCoordinator.start()
    }
}
