//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

protocol Coordinator {
    var childCoordinators: [Coordinator] { get }
    func start()
}
