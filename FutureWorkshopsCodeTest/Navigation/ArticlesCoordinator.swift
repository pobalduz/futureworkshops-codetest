//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit
import FutureWorkshopsCodeTestKit

protocol LogoutObserver: class {
    func didPerformLogout()
}

class ArticlesCoordinator: Coordinator {
    
    weak var observer: LogoutObserver?
        
    private(set) var childCoordinators: [Coordinator] = []
    
    private let rootViewController: ContainerViewController
    private let navigationController = UINavigationController()
    
    init(rootViewController: ContainerViewController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        let viewController = ArticlesViewController()
        let viewModel = ArticlesViewModel()
        viewController.articlesViewModel = viewModel
        viewModel.coordinator = self
        navigationController.pushViewController(viewController, animated: false)
        rootViewController.updateContentViewController(navigationController)
    }
    
    func logout() {
        observer?.didPerformLogout()
    }
    
    func showDetails(articleID: ID, title: String) {
        let viewController = ArticleDetailsViewController()
        let viewModel = ArticleDetailsViewModel(articleID: articleID, title: title)
        viewController.articleDetailsViewModel = viewModel
        navigationController.pushViewController(viewController, animated: true)
    }
}
