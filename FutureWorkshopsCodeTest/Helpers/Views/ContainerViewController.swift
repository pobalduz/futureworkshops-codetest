//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    private(set) var currentViewController: UIViewController
    
    init() {
        self.currentViewController = UIViewController()
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func loadView() {
        super.loadView()
        view.addSubview(currentViewController.view)
        addChild(currentViewController)
        currentViewController.didMove(toParent: self)
    }
    
    func updateContentViewController(_ viewController: UIViewController) {
        currentViewController.willMove(toParent: nil)
        view.addSubview(viewController.view)
        addChild(viewController)
        viewController.didMove(toParent: self)
        viewController.view.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            viewController.view.alpha = 1
            self.currentViewController.view.alpha = 0
        }) { _ in
            self.currentViewController.removeFromParent()
            self.currentViewController.view.removeFromSuperview()
            self.currentViewController = viewController
        }
    }
}

