//
//  Created by Pablo Balduz on 17/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

class ErrorView: UIStackView {
    
    private let messageLabel = UILabel()
    private let actionButton = UIButton(type: .system)
    
    private let actionHandler: () -> ()
    
    init(message: String, actionTitle: String, actionHandler: @escaping () -> ()) {
        self.actionHandler = actionHandler
        super.init(frame: .zero)
        
        layout()
        setupViews()
        messageLabel.text = message
        actionButton.setTitle(actionTitle, for: .normal)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc
    private func actionButtonTap() {
        actionHandler()
    }
    
    // MARK: - Private
    
    private func layout() {
        axis = .vertical
        spacing = 10
        [messageLabel, actionButton].forEach(addArrangedSubview)
    }
    
    private func setupViews() {
        errorViewLabelStyle(messageLabel)
        actionButton.addTarget(self, action: #selector(actionButtonTap), for: .touchUpInside)
    }
}
