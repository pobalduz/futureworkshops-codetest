//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

extension UIFont {
  func applyingWeight(_ weight: UIFont.Weight) -> UIFont {
    let newDescriptor = fontDescriptor.addingAttributes([.traits: [
      UIFontDescriptor.TraitKey.weight: weight]
    ])
    return UIFont(descriptor: newDescriptor, size: pointSize)
  }
}
