//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

extension UIViewController {
    
    private static let SpinnerTag = 81
    
    func showSpinner() {
        let spinner = UIActivityIndicatorView(style: .large)
        addContentView(spinner, backgroundColor: UIColor.black.withAlphaComponent(0.1))
        spinner.startAnimating()
    }
    
    func hideSpinner() {
        removeContentView()
    }
    
    func showAlert(title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorView(message: String, actionTitle: String, actionHandler: @escaping () -> ()) {
        hideErrorView()
        let errorView = ErrorView(message: message, actionTitle: actionTitle, actionHandler: actionHandler)
        addContentView(errorView, backgroundColor: .white)
    }
    
    func hideErrorView() {
        removeContentView()
    }
    
    private func addContentView(_ childView: UIView, backgroundColor: UIColor) {
        let child = ContentViewController(view: childView, backgroundColor: backgroundColor)
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.alpha = 0
        UIView.animate(withDuration: 0.3) {
            child.view.alpha = 1
        }
    }
    
    private func removeContentView() {
        guard let contentViewChild = children.compactMap({ $0 as? ContentViewController }).first else { return }
        contentViewChild.willMove(toParent: nil)
        contentViewChild.removeFromParent()
        UIView.animate(withDuration: 0.3, animations: {
            contentViewChild.view.alpha = 0
        }) { _ in
            contentViewChild.view.removeFromSuperview()
        }
    }
}

private class ContentViewController: UIViewController {
    
    let contentView: UIView
    let backgroundColor: UIColor
    
    init(view: UIView, backgroundColor: UIColor) {
        self.contentView = view
        self.backgroundColor = backgroundColor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = backgroundColor
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24)
        ])
    }
}
