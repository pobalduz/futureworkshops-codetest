//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImage(url: URL, placeholderImage: UIImage? = UIImage.imageWithColor(.lightGray)) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            let image: UIImage? = {
                guard
                    let data = data,
                    error == nil,
                    let response = response as? HTTPURLResponse,
                    (200..<300).contains(response.statusCode) else {
                        return placeholderImage
                }
                return UIImage(data: data)
            }()
            DispatchQueue.main.async { [weak self] in
                self?.image = image
            }
        }.resume()
    }
}
