//
//  Created by Pablo Balduz on 15/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import FutureWorkshopsCodeTestKit

// Based on Stephen Celis talk at NSSpain
// https://vimeo.com/291588126

var Current = World()

struct World {
    var apiClient = AppAPIClient(environment: API.AppEnvironment.develop)
    
    lazy var authProvider: AuthProviderType = AuthProvider(apiClient: apiClient)
    lazy var articlesProvider: ArticlesProviderType = ArticlesProvider(apiClient: apiClient)
}
