//
//  Created by Pablo Balduz on 15/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import FutureWorkshopsCodeTestKit

protocol ArticlesProviderType {
    func fetchArticles(completionHandler: @escaping (Result<[ArticlePreview], Error>) -> ())
    func fetchArticleDetails(id: ID, completionHandler: @escaping (Result<ArticleDetails, Error>) -> ())
    func setArticleAsFavorite(_ favorite: Bool, articleID: ID)
    func isArticleFavorite(_ articleID: ID) -> Bool
    func removeFavorites()
}

class ArticlesProvider: ArticlesProviderType {
    
    private let apiClient: AppAPIClient
    
    init(apiClient: AppAPIClient) {
        self.apiClient = apiClient
    }
    
    func fetchArticles(completionHandler: @escaping (Result<[ArticlePreview], Error>) -> ()) {
        apiClient.fetchArticles(completionHandler: completionHandler)
    }
    
    func fetchArticleDetails(id: ID, completionHandler: @escaping (Result<ArticleDetails, Error>) -> ()) {
        apiClient.fetchArticleDetails(id: id, completionHandler: completionHandler)
    }
    
    // For this we could create a Core Data manager and add an entity to store the favorite articles
    // or even cache the articles and store whether they are set as favorite or not
    // Since this a simple and concrete task I will store this information in user defaults and clean it whenever a logout is performed
    // Modifying it wouldn't be a problem as this provider conforms to a protocol, so as long as this protocol remains the same no changes will be needed in other parts of the code
    
    private let favoritesKey = "favorites"
    
    func setArticleAsFavorite(_ favorite: Bool, articleID: ID) {
        var currentFavorites = UserDefaults.standard.array(forKey: favoritesKey) as? [ID] ?? []
        if favorite, !currentFavorites.contains(articleID) {
            currentFavorites.append(articleID)
        } else {
            currentFavorites.removeAll(where: { $0 == articleID })
        }
        UserDefaults.standard.set(currentFavorites, forKey: favoritesKey)
        UserDefaults.standard.synchronize()
    }
    
    func isArticleFavorite(_ articleID: ID) -> Bool {
        guard let currentFavorites = UserDefaults.standard.array(forKey: favoritesKey) as? [ID] else { return false }
        return currentFavorites.contains(articleID)
    }
    
    func removeFavorites() {
        UserDefaults.standard.set(nil, forKey: favoritesKey)
        UserDefaults.standard.synchronize()
    }
}
