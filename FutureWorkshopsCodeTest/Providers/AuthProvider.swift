//
//  Created by Pablo Balduz on 15/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation
import FutureWorkshopsCodeTestKit

protocol AuthProviderType {
    var authToken: String? { get set }
    
    func login(userName: String, password: String, completionHandler: @escaping (Result<(), Error>) -> ())
    func logout()
}

extension AuthProviderType {
    
    var hasActiveSession: Bool {
        return authToken != nil
    }
}

class AuthProvider: AuthProviderType {
    
    var authToken: String? {
        get {
            Keychain.default.authToken()
        }
        set {
            Keychain.default.setAuthToken(newValue)
            if let token = newValue {
                apiClient.authToken = token
            }
        }
    }
    
    private let apiClient: AppAPIClient
    
    init(apiClient: AppAPIClient) {
        self.apiClient = apiClient
        if let token = authToken {
            apiClient.authToken = token
        }
    }
    
    func login(userName: String, password: String, completionHandler: @escaping (Result<(), Error>) -> ()) {
        apiClient.login(userName: userName, password: password) { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let response):
                self?.authToken = response.accessToken
                // if there was an endpoint to refresh credentials we would store refresh token as well
                completionHandler(.success(()))
            }
        }
    }
    
    func logout() {
        Keychain.default.clearKeychain()
        apiClient.authToken = nil
    }
}
