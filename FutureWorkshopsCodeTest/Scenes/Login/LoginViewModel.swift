//
//  Created by Pablo Balduz on 17/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import Foundation

protocol LoginViewModelType {
    var loginButtonIsEnabled: Dynamic<Bool> { get }
    var isLoading: Dynamic<Bool> { get }
    var errorMessage: Dynamic<String?> { get }
    func updateUserName(_ userName: String)
    func updatePassword(_ password: String)
    func performLogin()
}

class LoginViewModel: LoginViewModelType {
    
    weak var coordinator: AuthCoordinator?
    
    var loginButtonIsEnabled = Dynamic<Bool>(false)
    var isLoading = Dynamic<Bool>(false)
    var errorMessage = Dynamic<String?>(nil)
    
    private let authProvider: AuthProviderType
    private var userName: String = ""
    private var password: String = ""
    
    private var textFieldsAreFilled: Bool {
        return !userName.isEmpty && !password.isEmpty
    }
    
    init(authProvider: AuthProviderType = Current.authProvider) {
        self.authProvider = authProvider
    }
    
    func updatePassword(_ password: String) {
        self.password = password
        if loginButtonIsEnabled.value != textFieldsAreFilled {
            loginButtonIsEnabled.value = textFieldsAreFilled
        }
    }
    
    func updateUserName(_ userName: String) {
        self.userName = userName
        if loginButtonIsEnabled.value != textFieldsAreFilled {
            loginButtonIsEnabled.value = textFieldsAreFilled
        }
    }
    
    func performLogin() {
        guard !userName.isEmpty, !password.isEmpty else {
            // not necessary to show error as we only enable login button if text fields are already filled
            return
        }
        isLoading.value = true
        authProvider.login(userName: userName, password: password) { [weak self] result in
            self?.isLoading.value = false
            switch result {
            case .failure(let error):
                let message = (error as? LocalizedError)?.errorDescription
                self?.errorMessage.value = message
            case .success:
                self?.coordinator?.completeLogin()
            }
        }

    }
}
