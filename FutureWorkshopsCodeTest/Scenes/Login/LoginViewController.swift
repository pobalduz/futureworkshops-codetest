//
//  Created by Pablo Balduz on 15/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var viewModel: LoginViewModelType!
    
    private let imageView = UIImageView()
    private let userNameTextField = UITextField()
    private let passwordTextField = UITextField()
    private let loginButton = UIButton(type: .system)
    
    override func loadView() {
        super.loadView()
        layout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupBindings()
    }
    
    // MARK: Actions
    
    @objc
    private func loginButtonTap() {
        view.endEditing(true)
        viewModel.performLogin()
    }
    
    @objc
    private func userNameTextFieldChanged() {
        guard let text = userNameTextField.text else { return }
        viewModel.updateUserName(text)
    }
    
    @objc
    private func passwordTextFieldChanged() {
        guard let text = passwordTextField.text else { return }
        viewModel.updatePassword(text)
    }
    
    // MARK: - Private
    
    private func layout() {
        let formContentView = UIStackView(arrangedSubviews: [userNameTextField, passwordTextField, loginButton])
        formContentView.axis = .vertical
        formContentView.spacing = 24
        formContentView.isLayoutMarginsRelativeArrangement = true
        formContentView.layoutMargins = UIEdgeInsets(top: 10, left: 24, bottom: 10, right: 24)
        
        let contentView = UIStackView(arrangedSubviews: [imageView, formContentView, UIView()])
        contentView.axis = .vertical
        contentView.spacing = 36
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(contentView)
        contentView.pinToSuperview()
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3)
        ])
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        fillImageViewStyle(imageView)
        imageView.loadImage(url: URL(string: "https://mobilecodetest.fws.dev/images/ipad.jpg")!)
        userNameTextFieldStyle("Username")(userNameTextField)
        userNameTextField.addTarget(self, action: #selector(userNameTextFieldChanged), for: .editingChanged)
        passwordTextFieldStyle("Password")(passwordTextField)
        passwordTextField.addTarget(self, action: #selector(passwordTextFieldChanged), for: .editingChanged)
        loginButton.setTitle("Login", for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonTap), for: .touchUpInside)
    }
    
    private func setupBindings() {
        viewModel.loginButtonIsEnabled.bindAndFire { [weak self] isEnabled in
            self?.loginButton.isEnabled = isEnabled
        }
        viewModel.isLoading.bind { [weak self] isLoading in
            if isLoading {
                self?.showSpinner()
            } else {
                self?.hideSpinner()
            }
        }
        viewModel.errorMessage.bind { [weak self] message in
            self?.showAlert(title: "Error", message: message)
        }
    }
}
