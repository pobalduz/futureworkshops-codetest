//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit
import FutureWorkshopsCodeTestKit

class ArticleDetailsViewController: UIViewController {
    
    var articleDetailsViewModel: ArticleDetailsViewModelType!
    
    private let scrollView = UIScrollView()
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let dateLabel = UILabel()
    private let contentLabel = UILabel()
    private var starButton: UIBarButtonItem!
    
    override func loadView() {
        super.loadView()
        layout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        setupViews()
        setupBindings()
        articleDetailsViewModel.viewDidLoad()
    }
    
    // MARK: - Actions
    
    @objc
    private func starButtonTap() {
        articleDetailsViewModel.toggleFavorite()
    }
    
    // MARK: - Private
    
    private func layout() {
        let textContentView = UIStackView(arrangedSubviews: [titleLabel, dateLabel, contentLabel])
        textContentView.axis = .vertical
        textContentView.spacing = 10
        textContentView.isLayoutMarginsRelativeArrangement = true
        textContentView.layoutMargins = UIEdgeInsets(top: 8, left: 10, bottom: 20, right: 8)
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        dateLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        let contentView = UIStackView(arrangedSubviews: [imageView, textContentView])
        contentView.axis = .vertical
        contentView.spacing = 10
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.pinToSuperviewTopSafeArea()
        scrollView.addSubview(contentView)
        contentView.pinToSuperview()
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
    }
    
    private func setupNavBar() {
        starButton = UIBarButtonItem(image: UIImage(systemName: "star"), style: .plain, target: self, action: #selector(starButtonTap))
        navigationItem.rightBarButtonItem = starButton
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        boldTitleLabelStyle(titleLabel)
        summaryDetailLabelStyle(contentLabel)
        dateLabelStyle(dateLabel)
        fillImageViewStyle(imageView)
    }
    
    private func setupBindings() {
        articleDetailsViewModel.isFavorite.bindAndFire { [weak self] isFavorite in
            self?.starButton.image = UIImage(systemName: isFavorite ? "star.fill" : "star")
        }
        articleDetailsViewModel.isLoading.bind { [weak self] isLoading in
            if isLoading {
                self?.showSpinner()
            } else {
                self?.hideSpinner()
            }
        }
        articleDetailsViewModel.error.bind { [weak self] errorTuple in
            self?.showErrorView(message: errorTuple.0, actionTitle: errorTuple.1, actionHandler: {
                self?.hideErrorView()
                self?.viewDidLoad()
            })
        }
        articleDetailsViewModel.details.bind { [weak self] details in
            guard let details = details else { return }
            self?.titleLabel.text = details.title
            self?.dateLabel.text = dateDisplayFormatter.string(from: details.date)
            self?.contentLabel.text = details.content
            self?.imageView.loadImage(url: details.imageURL)
        }
    }
}
