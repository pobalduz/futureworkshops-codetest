//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit
import FutureWorkshopsCodeTestKit

class ArticlesViewController: UITableViewController {
    
    var articlesViewModel: ArticlesViewModelType!
            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        setupTableView()
        setupBindings()
        articlesViewModel.viewDidLoad()
    }
    
    // MARK: - Actions
    
    @objc
    private func logoutButtonTap() {
        articlesViewModel.didSelectLogout()
    }
    
    // MARK: - UITableViewDataSource & UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articlesViewModel.articles.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticlePreviewCell.reuseIdentifier, for: indexPath) as! ArticlePreviewCell
        cell.configure(for: articlesViewModel.articles.value[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        articlesViewModel.didSelectArticle(index: indexPath.row)
    }
    
    // MARK: - Private
    
    private func setupNavBar() {
        title = "Articles"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutButtonTap))
    }
    
    private func setupTableView() {
        tableView.register(ArticlePreviewCell.self, forCellReuseIdentifier: ArticlePreviewCell.reuseIdentifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 120, bottom: 0, right: 30)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupBindings() {
        articlesViewModel.articles.bind { [weak self] _ in
            self?.tableView.reloadData()
        }
        articlesViewModel.error.bind { [weak self] errorTuple in
            self?.showErrorView(message: errorTuple.0, actionTitle: errorTuple.1, actionHandler: {
                self?.hideErrorView()
                self?.viewDidLoad()
            })
        }
        articlesViewModel.isLoading.bind { [weak self] isLoading in
            if isLoading {
                self?.showSpinner()
            } else {
                self?.hideSpinner()
            }
        }
    }
}
