//
//  Created by Pablo Balduz on 18/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import FutureWorkshopsCodeTestKit

protocol ArticlesViewModelType {
    var articles: Dynamic<[ArticlePreview]> { get }
    var error: Dynamic<(String, String)> { get }
    var isLoading: Dynamic<Bool> { get }
    func viewDidLoad()
    func didSelectArticle(index: Int)
    func didSelectLogout()
}

class ArticlesViewModel: ArticlesViewModelType {
    
    weak var coordinator: ArticlesCoordinator?
    
    var articles = Dynamic<[ArticlePreview]>([])
    var error = Dynamic<(String, String)>(("", ""))
    var isLoading = Dynamic<Bool>(false)
    
    private let articlesProvider: ArticlesProviderType
    
    init(articlesProvider: ArticlesProviderType = Current.articlesProvider) {
        self.articlesProvider = articlesProvider
    }
    
    func viewDidLoad() {
        isLoading.value = true
        articlesProvider.fetchArticles { [weak self] result in
            self?.isLoading.value = false
            switch result {
            case .failure:
                self?.error.value = ("Seems like data couldn't be loaded", "Retry")
            case .success(let articles):
                self?.articles.value = articles
            }
        }
    }
    
    func didSelectArticle(index: Int) {
        let article = articles.value[index]
        coordinator?.showDetails(articleID: article.id, title: "Article \(index + 1)")
    }
    
    func didSelectLogout() {
        Current.authProvider.logout()
        Current.articlesProvider.removeFavorites()
        coordinator?.logout()
    }
}
