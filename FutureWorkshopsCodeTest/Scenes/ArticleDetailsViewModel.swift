//
//  Created by Pablo Balduz on 18/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import FutureWorkshopsCodeTestKit

protocol ArticleDetailsViewModelType {
    var details: Dynamic<ArticleDetails?> { get }
    var isLoading: Dynamic<Bool> { get }
    var error: Dynamic<(String, String)> { get }
    var isFavorite: Dynamic<Bool> { get }
    var title: String { get }
    func viewDidLoad()
    func toggleFavorite()
}

class ArticleDetailsViewModel: ArticleDetailsViewModelType {
    
    var details = Dynamic<ArticleDetails?>(nil)
    var isLoading = Dynamic<Bool>(false)
    var error = Dynamic<(String, String)>(("", ""))
    var isFavorite = Dynamic<Bool>(false)
    var title: String
    
    private let articleID: ID
    private let articlesProvider: ArticlesProviderType
    
    init(articleID: ID, title: String, provider: ArticlesProviderType = Current.articlesProvider) {
        self.articleID = articleID
        self.title = title
        self.articlesProvider = provider
    }
    
    func viewDidLoad() {
        isLoading.value = true
        articlesProvider.fetchArticleDetails(id: articleID) { [weak self] result in
            self?.isLoading.value = false
            switch result {
            case .failure:
                self?.error.value = ("Seems like data couldn't be loaded", "Retry")
            case .success(let details):
                self?.details.value = details
            }
        }
        isFavorite.value = articlesProvider.isArticleFavorite(articleID)
    }
    
    func toggleFavorite() {
        articlesProvider.setArticleAsFavorite(!isFavorite.value, articleID: articleID)
        isFavorite.value.toggle()
    }
}
