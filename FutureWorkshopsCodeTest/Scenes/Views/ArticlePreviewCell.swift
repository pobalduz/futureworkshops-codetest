//
//  Created by Pablo Balduz on 16/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit
import FutureWorkshopsCodeTestKit

class ArticlePreviewCell: UITableViewCell {
    
    private let thumbnailImageView = UIImageView()
    private let dateLabel = UILabel()
    private let articleTitleLabel = UILabel()
    private let articleSummaryLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        layout()
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(for article: ArticlePreview) {
        dateLabel.text = dateDisplayFormatter.string(from: article.date)
        articleTitleLabel.text = article.title
        articleSummaryLabel.text = article.summary
        thumbnailImageView.loadImage(url: article.thumbnailURL)
    }
    
    // MARK: - Private
    
    private func layout() {
        let thumbnailContentView = UIStackView(arrangedSubviews: [thumbnailImageView, dateLabel, UIView()])
        thumbnailContentView.axis = .vertical
        thumbnailContentView.spacing = 8
        
        let textContentView = UIStackView(arrangedSubviews: [articleTitleLabel, articleSummaryLabel])
        textContentView.axis = .vertical
        textContentView.spacing = 8
        articleTitleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        let contentStackView = UIStackView(arrangedSubviews: [thumbnailContentView, textContentView])
        contentStackView.spacing = 24
        contentStackView.isLayoutMarginsRelativeArrangement = true
        contentStackView.layoutMargins = UIEdgeInsets(top: 12, left: 30, bottom: 12, right: 30)
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(contentStackView)
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            thumbnailImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.2),
            thumbnailImageView.heightAnchor.constraint(equalTo: thumbnailImageView.widthAnchor)
        ])
    }
    
    private func setupViews() {
        selectionStyle = .none
        previewImageViewStyle(thumbnailImageView)
        dateLabelStyle(dateLabel)
        titleLabelStyle(articleTitleLabel)
        summaryPreviewLabelStyle(articleSummaryLabel)
    }
}

extension UITableViewCell {
    
    static var reuseIdentifier: String {
        String(describing: self)
    }
}

var dateDisplayFormatter: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yy"
    return formatter
}
