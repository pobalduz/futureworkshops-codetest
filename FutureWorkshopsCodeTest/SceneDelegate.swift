//
//  SceneDelegate.swift
//  FutureWorkshopsCodeTest
//
//  Created by Pablo Balduz on 12/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    private var appCoordinator: AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        defer { window?.makeKeyAndVisible() }
        window?.windowScene = scene
        
        guard NSClassFromString("XCTest") == nil else {
            window?.rootViewController = UIViewController()
            return
        }
        
        appCoordinator = AppCoordinator(window: window!)
        appCoordinator?.start()
        Current.apiClient.delegate = appCoordinator
    }
}
