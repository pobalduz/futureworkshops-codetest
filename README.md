# FutureWorkshops-CodeTest

This repository includes two branches, one simpler than the other. In the **master** branch all the logic happening in each screen is in the view controller as this app is very simple and doesn't have that much logic. 

On the other hand, you'll find a cleaner system in the **view-model** branch where the view controllers have an associated view model that manages its logic. I have included both versions so the differences can be seen.

The app navigation is performed using coordinators. This system simplifies managing navigation depending on whether the user is logged in or not and frees the view controllers from this task.

You'll find as well a different module than the main one inside the app, this module has all the networking layer. It contains the wrappers used to interact with an API and the API implementation used for this application.

Lastly there are some things that we could still improve:

- Localization of strings
- Creation of the final models to use in the app instead of depending on the ones returned by the server (this case was a simple one, that's why they were used directly)
- Add a Core Data wrapper that we can inject in the articles provider to create an entity to store favorite articles or even cache them as well
