//
//  Created by Pablo Balduz on 17/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsCodeTest

class LoginViewModelTests: XCTestCase {
    
    private var viewModel: LoginViewModel!

    override func setUp() {
        super.setUp()
        viewModel = LoginViewModel(authProvider: FakeAuthProvider())
    }

    func testLoginButtonEnabledOnlyUsername() {
        viewModel.updateUserName("username")
        XCTAssertFalse(viewModel.loginButtonIsEnabled.value)
    }
    
    func testLoginButtonEnabledOnlyPassword() {
        viewModel.updatePassword("password")
        XCTAssertFalse(viewModel.loginButtonIsEnabled.value)
    }
    
    func testLoginButtonEnabled() {
        viewModel.updateUserName("username")
        viewModel.updatePassword("password")
        XCTAssertTrue(viewModel.loginButtonIsEnabled.value)
    }
    
    struct FakeAuthProvider: AuthProviderType {
        var authToken: String?
        func login(userName: String, password: String, completionHandler: @escaping (Result<(), Error>) -> ()) { }
        func logout() { }
    }
}
