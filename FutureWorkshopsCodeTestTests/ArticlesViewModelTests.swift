//
//  Created by Pablo Balduz on 18/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import XCTest
import FutureWorkshopsCodeTestKit
@testable import FutureWorkshopsCodeTest

class ArticlesViewModelTests: XCTestCase {
    
    func testFetchArticlesError() {
        let viewModel = ArticlesViewModel(articlesProvider: FakeArticlesProvider(error: true))
        viewModel.error.bind { error in
            XCTAssertFalse(viewModel.error.value.0.isEmpty)
            XCTAssertFalse(viewModel.error.value.1.isEmpty)
        }
        viewModel.viewDidLoad()
    }
    
    func testFetchArticlesOk() {
        let viewModel = ArticlesViewModel(articlesProvider: FakeArticlesProvider(error: false))
        viewModel.articles.bind { articles in
            XCTAssertTrue(articles.count > 0)
        }
        viewModel.viewDidLoad()
    }
}
