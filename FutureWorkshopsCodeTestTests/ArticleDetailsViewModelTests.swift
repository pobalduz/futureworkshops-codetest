//
//  Created by Pablo Balduz on 18/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsCodeTest

class ArticleDetailsViewModelTests: XCTestCase {
    
    func testFetchDetailsError() {
        let viewModel = ArticleDetailsViewModel(articleID: 24, title: "Black Mamba", provider: FakeArticlesProvider(error: true))
        viewModel.error.bind { error in
            XCTAssertFalse(viewModel.error.value.0.isEmpty)
            XCTAssertFalse(viewModel.error.value.1.isEmpty)
        }
        viewModel.viewDidLoad()
    }
    
    func testFetchArticlesOk() {
        let viewModel = ArticleDetailsViewModel(articleID: 24, title: "Black Mamba", provider: FakeArticlesProvider(error: false))
        viewModel.details.bind { details in
            XCTAssertNotNil(details)
        }
        viewModel.viewDidLoad()
    }
    
    func testToggleFavorite() {
        let viewModel = ArticleDetailsViewModel(articleID: 24, title: "Black Mamba", provider: FakeArticlesProvider(error: false))
        let currentFavoriteValue = viewModel.isFavorite.value
        viewModel.isFavorite.bind { isFavorite in
            XCTAssert(currentFavoriteValue == !isFavorite)
        }
    }
}
