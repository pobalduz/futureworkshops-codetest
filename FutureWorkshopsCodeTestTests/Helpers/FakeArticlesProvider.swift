//
//  Created by Pablo Balduz on 18/06/2020.
//  Copyright © 2020 Pablo Balduz. All rights reserved.
//

@testable import FutureWorkshopsCodeTest
import FutureWorkshopsCodeTestKit

class FakeArticlesProvider: ArticlesProviderType {
    
    private let shouldFailRequest: Bool
    private var isFavorite = false
    
    init(error: Bool) {
        shouldFailRequest = error
    }
    
    func fetchArticles(completionHandler: @escaping (Result<[ArticlePreview], Error>) -> ()) {
        if shouldFailRequest {
            completionHandler(.failure(FakeError()))
        } else {
            completionHandler(.success([ArticlePreview(id: 24,
                                                       date: Date(),
                                                       summary: "summary",
                                                       title: "title",
                                                       thumbnailTemplateURL: URL(string: "www.nba.com")!,
                                                       thumbnailURL: URL(string: "www.nba.com")!)]))
        }
    }
    
    func fetchArticleDetails(id: ID, completionHandler: @escaping (Result<ArticleDetails, Error>) -> ()) {
        if shouldFailRequest {
            completionHandler(.failure(FakeError()))
        } else {
            completionHandler(.success(ArticleDetails(id: 24,
                                                      date: Date(),
                                                      summary: "summary",
                                                      title: "title",
                                                      thumbnailTemplateURL: URL(string: "www.nba.com")!,
                                                      thumbnailURL: URL(string: "www.nba.com")!,
                                                      content: "content",
                                                      imageURL: URL(string: "www.nba.com")!,
                                                      sourceURL: URL(string: "www.nba.com")!)))
        }
    }
    
    func setArticleAsFavorite(_ favorite: Bool, articleID: ID) {
        isFavorite.toggle()
    }
    func isArticleFavorite(_ articleID: ID) -> Bool {
        return isFavorite
    }
    func removeFavorites() { }
}

struct FakeError: Error { }
